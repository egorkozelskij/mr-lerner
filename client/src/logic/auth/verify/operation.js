import * as actions from './action';
import { getFromLocalStorage } from 'libs/webStorage';
import wrapper from 'libs/funcWrapper';

const verifyUser = () => {
	return dispatch => {
		dispatch(actions.verifyRequest());
		
		wrapper(() => {
			const authData = getFromLocalStorage('auth');
			if (!authData || !authData.token) {
				dispatch(actions.verifyFail());
				return;
			}
		
			fetch('/api/auth/verify?token=' + authData.token)
			.then(res => res.json())
			.then(json => {
				if (json.success) {
					dispatch(actions.verifySuccess(json.user));
				} else {
					dispatch(actions.verifyFail());
				}
			})
			.catch(() => dispatch(actions.verifyFail()));
		});
	}
};
export default verifyUser;