const VERIFY_REQUEST = "auth/verifyRequest";
const VERIFY_SUCCESS = "auth/verifySuccess";
const VERIFY_FAIL    = "auth/verifyFail";

export {
	VERIFY_REQUEST,
	VERIFY_SUCCESS,
	VERIFY_FAIL
};