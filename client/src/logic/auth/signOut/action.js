import * as types from './type';


export const signOutRequest = () => {
	return {
		type: types.SIGNOUT_REQUEST,
		payload: {
			isAuth: false,
			isFetching: false
		}
	}
};