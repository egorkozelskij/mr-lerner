import * as verifyTypes from "./verify/type";
import * as signInTypes from "./signIn/type";
import * as signOutTypes from "./signOut/type";
import * as signUpTypes from "./signUp/type";
const types = Object.assign({}, verifyTypes, signInTypes, signOutTypes, signUpTypes);

const initialState = {
	isAuth: false,
	isFetching: true
};

function authReducer(state = initialState, action) {
	switch (action.type) {
		case types.VERIFY_SUCCESS:
		case types.VERIFY_REQUEST:
		case types.VERIFY_FAIL:
		case types.SIGNIN_REQUEST:
		case types.SIGNIN_SUCCESS:
		case types.SIGNIN_FAIL:
		case types.SIGNOUT_REQUEST:
		case types.SIGNUP_REQUEST:
		case types.SIGNUP_SUCCESS:
		case types.SIGNUP_FAIL:
			const {user, errorMessage, signInSuccess, signUpSuccess, ...partState} = state;
			return Object.assign({}, partState, action.payload);
		default:
			return state;
	}
}
export default authReducer;
