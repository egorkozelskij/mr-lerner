const SIGNUP_REQUEST = 'auth/signUpRequest';
const SIGNUP_SUCCESS = 'auth/signUpSuccess';
const SIGNUP_FAIL = 'auth/signUpFail';

export {
	SIGNUP_REQUEST,
	SIGNUP_SUCCESS,
	SIGNUP_FAIL
};