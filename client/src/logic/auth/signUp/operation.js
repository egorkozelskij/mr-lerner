import * as actions from './action';
import wrapper from 'libs/funcWrapper';

const signUpUser = (creds) => {
	return dispatch => {
		dispatch(actions.signUpRequest());
		if (creds.password !== creds.passwordAgain) {
			dispatch(actions.signUpFail('Passwords is not the same'));
			return;
		}
		
		wrapper(() => {
			fetch('/api/auth/signup', {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json'
				},
				body: JSON.stringify({
					email: creds.email,
					password: creds.password
				})
			})
			.then(res => res.json())
			.then(json => {
				if (json.success) {
					dispatch(actions.signUpSuccess());
				} else {
					dispatch(actions.signUpFail(json.message));
				}
			})
			.catch(() => {
				dispatch(actions.signUpFail("Unknown error"));
			})
		});
	}
};
export default signUpUser;