import * as types from './type';

export const signUpRequest = () => {
	return {
		type: types.SIGNUP_REQUEST,
		payload: {
			isAuth: false,
			isFetching: true
		}
	}
};

export const signUpSuccess= () => {
	return {
		type: types.SIGNUP_SUCCESS,
		payload: {
			isAuth: false,
			isFetching: false,
			signUpSuccess: true
		}
	}
};

export const signUpFail = (message) => {
	return {
		type: types.SIGNUP_FAIL,
		payload: {
			isAuth: false,
			isFetching: false,
			signUpSuccess: false,
			errorMessage: message
		}
	}
};