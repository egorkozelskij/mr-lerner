const wrapper = (func) => {
	setTimeout(func, 100);
};

export default wrapper;