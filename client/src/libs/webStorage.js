export function getFromLocalStorage(key) {
	if (!key) {
		return null;
	}
	try {
		const valueStr = localStorage.getItem(key);
		if (valueStr) {
			return JSON.parse(valueStr);
		}
		return null;
	} catch (err) {
		return null;
	}
}
export function setInLocalStorage(key, obj) {
	if (!key) {
		console.error('Error: Key is missing');
	}
	try {
		localStorage.setItem(key, JSON.stringify(obj));
	} catch (err) {
		console.error(err);
	}
}

export function getFromSessionStorage(key) {
	if (!key) {
		return null;
	}
	try {
		const valueStr = sessionStorage.getItem(key);
		if (valueStr) {
			return JSON.parse(valueStr);
		}
		return null;
	} catch (err) {
		return null;
	}
}
export function setInSessionStorage(key, obj) {
	if (!key) {
		console.error('Error: Key is missing');
	}
	try {
		sessionStorage.setItem(key, JSON.stringify(obj));
	} catch (err) {
		console.error(err);
	}
}