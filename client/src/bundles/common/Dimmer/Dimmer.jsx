import React from 'react';
import PropTypes from 'prop-types';

import './style.scss';

const Dimmer = ({duration, color, func, hidden, children}) => {
	
	let style = {};
	
	if (color)
		style.backgroundColor = color;
	
	if (duration)
		style.animationDuration = duration.toString() + 's';
	
	if (func)
		style.animationTimingFunction = func;
	
	return (
		<div className={hidden?'dimmer-hidden':'dimmer'} style={style}>
			{children}
		</div>
	);
};

Dimmer.propTypes = {
	duration: PropTypes.number,
	color: PropTypes.string,
	func: PropTypes.string,
	hidden: PropTypes.bool
};
 
export default Dimmer;
