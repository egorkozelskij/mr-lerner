import React from 'react';
import PropTypes from 'prop-types';

const Message = ({type, message}) => {
	const style = {
		borderRadius: '0'
	};
	return (
		<div style={style} className={"message-block alert error-message " + "alert-"+type}>
			{message}
		</div>
	);
};
Message.propTypes = {
	message: PropTypes.string.isRequired,
	type: PropTypes.string.isRequired
};
export default Message;
