import React from 'react';
import PropTypes from 'prop-types';

const Flag = ({width,county}) => {
	const k = 46/62;
	if (!width)
		width = 63;
	const style = {
		width: `${width}px`,
		height: `${width*k}px`,
		borderRadius: '0%',
		border: '0px solid #c5c5c5',
		backgroundColor: 'transparent',
		display: 'inline-block'
	};
	
	return (
		<div style={style} className={"flag-icon-background flag-icon-squared flag-icon-"+county}/>
	);
};
Flag.propTypes = {
	width: PropTypes.number,
	county: PropTypes.string.isRequired
};
export default Flag;
