import React from 'react';
import {Container, Row, Col} from "reactstrap";
import PropTypes from 'prop-types';

import './style.scss';

const MainWrapper = ({LeftComponent, TopComponent, MiddleComponent}) => (
	<React.Fragment>
		<Container fluid className='page-main-wrapper'>
			<Container fluid>
					{TopComponent? <TopComponent/> : ''}
			</Container>
			<Container fluid>
				<Row>
					<Col className='d-none d-lg-block offset-lg-0 offset-xl-1' lg={2} xl={2}>
						<div className='d-flex justify-content-end'>
							{LeftComponent? <LeftComponent/> : ''}
						</div>
					</Col>
					<Col className='col-12 p-0' xs={12} sm={12} md={12} lg={10} xl={7}>
						{TopComponent? <hr/> : ''}
						{MiddleComponent? <MiddleComponent/> : ''}
					</Col>
				</Row>
			</Container>
		</Container>
	</React.Fragment>
 );
MainWrapper.propTypes = {
	LeftComponent: PropTypes.any,
	TopComponent:  PropTypes.any,
	MiddleComponent:  PropTypes.any
};
export default MainWrapper;
