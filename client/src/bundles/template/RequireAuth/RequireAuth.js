import React, {Component} from 'react';
import {Redirect} from "react-router";
import {connect} from "react-redux";
import PropTypes from 'prop-types';

import {DimmerLoader, DimmerLoaderContainer} from "common/DimmerLoader";

class RequireAuth extends Component {
	render() {
		let Dimmer = () => ('');
		if (this.props.auth.isFetching)
			Dimmer = DimmerLoader;
		
		if (this.props.auth.isFetching)
			return (
				<DimmerLoaderContainer>
					<DimmerLoader/>
				</DimmerLoaderContainer>
			);
		if (!this.props.auth.isAuth)
			return <Redirect to='/auth'/>;
			
		const ChildComponent = this.props.component;
		return (
			<ChildComponent/>
		);
	}
}
RequireAuth.propTypes = {
	auth: PropTypes.shape({
		isFetching: PropTypes.bool.isRequired,
		isAuth: PropTypes.bool.isRequired
	}),
	component: PropTypes.any.isRequired
};
const mapStateToProps = (state) => {
	return {
		auth: state.auth
	};
};
export default connect(mapStateToProps, null)(RequireAuth);
