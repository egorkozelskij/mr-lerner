import React from 'react';
import { Route, Switch } from 'react-router-dom';
import {withRouter} from "react-router";
import PropTypes from "prop-types";

import router from "config/routes";


const ContentRouter = ({_router}) => {
	let comRouter;
	if (!_router && _router === undefined)
		comRouter = router;
	else
		comRouter = _router;
		
	return (
		<Switch>
			{comRouter.common.map((route, ind) => {
				return (
					<Route key={ind} exact={route.exact} path={route.path}
					       component={route.component}
					/>
				);
			})}
			<Route component={comRouter.error.component}/>
		</Switch>
	);
};
ContentRouter.propTypes = {
	_router: PropTypes.object
};
export default withRouter(ContentRouter);
