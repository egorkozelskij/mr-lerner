import React, { Component } from 'react';
import {Route, withRouter} from 'react-router';
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import PropTypes from 'prop-types';
import {Container} from "reactstrap";
import {Switch} from "react-router-dom";

import Header from "template/Header";
import ContentRouter from "template/ContentRouter";
import Error404 from "error/Error404";

import {verifyUser} from 'logic/auth';

//GLOBAL PROJECT STYLES AND SCRIPTS
// import 'jquery/dist/jquery.min';
// import 'popper.js';
import '@fortawesome/fontawesome-free/js/all.min.js';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.min';
import 'designmodo-flat-ui/dist/css/flat-ui.min.css';
import 'flag-icon-css/css/flag-icon.min.css';
import 'select2/dist/js/select2.min';
import 'select2/dist/css/select2.min.css';

import 'template/styles';
import './style.scss';


class App extends Component {
	componentDidMount() {
		this.props.verifyUser();
	}
    render() {
        return (
	        <React.Fragment>
		        <Header/>
		        <Container fluid id='app-wrapper' className='pl-0 pr-0'>
			        <Switch>
			            <Route path='/' component={ContentRouter}/>
			            <Route component={Error404}/>
			        </Switch>
		        </Container>
	        </React.Fragment>
        );
    }
}

App.propTypes = {
	verifyUser: PropTypes.func.isRequired
};

const mapDispatchToProps = (dispatch) => {
	return bindActionCreators({verifyUser}, dispatch);
};
export default withRouter(connect(null, mapDispatchToProps)(App));
