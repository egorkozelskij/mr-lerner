import React, { Component } from 'react';
import {Link} from "react-router-dom";

import {routeTo} from "config/routes";
import './style.scss';
import cloudImg from './cloud.png';

class Error404 extends Component {
    render() {
        return (
          <div id='error404-wrapper'>
	          <div id='error404-middle-wrapper' className='container-fluid d-flex justify-content-center'>
		          <div>
			          <div className="error404-heading error404-heading1">404</div>
			          <hr/>
			          <div className="error404-heading error404-heading2">THE PAGE</div>
			          <div className="error404-heading error404-heading3">WAS NOT FOUND</div>
			          <div className='d-flex justify-content-center'>
				          <Link to={routeTo('home')} className="btn btn-hg btn-primary" id='error404-button'>
					          BACK TO HOME
				          </Link>
			          </div>
		          </div>
	          </div>
	          <img src={cloudImg} id="error-cloud1"/>
	          <img src={cloudImg} id="error-cloud2"/>
	          <img src={cloudImg} id="error-cloud3"/>
          </div>
        );
    }
}

export default Error404;
