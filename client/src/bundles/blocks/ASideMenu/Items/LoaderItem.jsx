import React from 'react';
import PropTypes from 'prop-types';
import Loader from "common/Loader/Loader";

const LoaderItem = ({color, size}) => (
	<div className="input-group input-loader-item">
		<Loader size={size?size:'30px'} color={color}/>
	</div>
);
LoaderItem.propTypes = {
	color: PropTypes.string,
	size: PropTypes.string
};
export default LoaderItem;
