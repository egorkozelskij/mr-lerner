import React from 'react';
import {Button} from "reactstrap";
import {connect} from "react-redux";
import PropTypes from 'prop-types';

import {ASideMenu, InputItem, InputDropdownItem, LoaderItem} from "blocks/ASideMenu";
import Message from "common/Message";
import getSassVar from "libs/getSassVar";

import './style.scss';

const MenuPresenter = ({onSave, onPropChange, LangLoading, dicts, data}) => (
	<ASideMenu title='Add dictionary'>
		<InputItem placeholder = 'Name' onChange={(d) => {
			onPropChange('name', d.target.value)
		}}/>
		{LangLoading ?
			<LoaderItem color={getSassVar('main-bg')}/>
			:
			<InputDropdownItem data={data}
			                   placeholder='Language'
			                   onSelect={(data) => {
			                   	onPropChange('lang', data.text);
			                   	onPropChange('code', data.id);
			                   }} />
		}
		{dicts.isSuccess !== undefined && dicts.message !== undefined && !dicts.isSuccess ?
			<div className='dict-menu-message-box'>
				<Message type='danger' message={dicts.message}/>
			</div>
			: ''
		}
		<Button onClick={onSave} disabled={LangLoading || dicts.isFetching}>SAVE</Button>
	</ASideMenu>
 );
MenuPresenter.propTypes = {
	onSave: PropTypes.func.isRequired,
	onPropChange: PropTypes.func.isRequired,
	LangLoading: PropTypes.bool.isRequired,
	data: PropTypes.array.isRequired,
	dicts: PropTypes.shape({
		isFetching: PropTypes.bool.isRequired
	}).isRequired
};
const mapStateToProps = (state) => {
	return {
		dicts: state.dicts
	};
};
export default connect(mapStateToProps, null)(MenuPresenter);
