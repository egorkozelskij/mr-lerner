const data = [
	"I want it!",
	"Give me more!",
	"Learn it!",
	"Take a look"
];

export default () => {
	const ind = Math.floor(Math.random() * Math.floor(data.length));
	return data[ind];
};