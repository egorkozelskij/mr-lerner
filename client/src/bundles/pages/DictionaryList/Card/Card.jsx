import React from 'react';
import PropTypes from 'prop-types';
import {Link, withRouter} from "react-router-dom";
import generateWord from './DictButtonWordsGenerator';

import './style.scss';
import './imageNotFound.png';
import Flag from "common/Flag";

const Card = ({name, lang, code, id, match}) => {
	return (
		<div className='dict-card-wrapper'>
			<div className="card dict-card">
				{code ?
					<Flag county={code} width={200}/>
					:
					<div className="dict-default-image"/>
				}
				
				<div className="card-body">
					<div className="card-title">{name}</div>
					<div className="card-lang">Language: {lang}</div>
					<div className='dict-card-panel'>
						<span>Language: {lang}</span>
						<Link to={`${match.url}/${id}`} className="btn btn-primary">{generateWord()}</Link>
					</div>
				</div>
			</div>
		</div>
	);
};
Card.propTypes = {
	name: PropTypes.string.isRequired,
	lang: PropTypes.string.isRequired,
	code: PropTypes.string,
	id: PropTypes.string.isRequired
};
export default withRouter(Card);
