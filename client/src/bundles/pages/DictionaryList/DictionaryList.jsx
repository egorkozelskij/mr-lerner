import React, { Component } from 'react';
import {Route, Switch, withRouter} from "react-router-dom";

import MainWrapper from 'template/MainWrapper';
import Menu from './Menu';
import CardsWrapper from "./CardsWrapper";

import './style.scss';
import Dictionary from "../Dictionary";

class DictionaryList extends Component {
	constructor(props) {
		super(props);
	}
    render() {
	    const LeftComponent = () => (<Menu/>);
	    const {match} = this.props;
	    const path = match.url;
	    return (
	    	<React.Fragment>
			    <Switch>
				    <Route path={`${match.path}/`} exact component={() => (
					    <MainWrapper MiddleComponent={CardsWrapper}
					                 LeftComponent={LeftComponent}/>
				    )}/>
				    <Route path={`${match.path}/:dictId/:option?`} component={withRouter(({match}) => {
					    const { dictId, option } = match.params;
				        return (
				            <Dictionary dictId={dictId} option={option} path={path}/>
					    );
				    })}/>
			    </Switch>
		    </React.Fragment>
	    );
    }
}
export default withRouter(DictionaryList);
