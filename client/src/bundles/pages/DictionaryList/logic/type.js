const ADDDICT_REQUEST = 'dicts/addRequest';
const ADDDICT_SUCCESS = 'dicts/addSuccess';
const ADDDICT_FAIL = 'dicts/addFail';

const GETDICTS_REQUEST = 'dicts/getRequest';
const GETDICTS_SUCCESS = 'dicts/getSuccess';
const GETDICTS_FAIL = 'dicts/getFail';

export {
	ADDDICT_REQUEST,
	ADDDICT_SUCCESS,
	ADDDICT_FAIL,
	
	GETDICTS_REQUEST,
	GETDICTS_SUCCESS,
	GETDICTS_FAIL
};