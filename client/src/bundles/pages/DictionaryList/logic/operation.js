import * as actions from './action';
import wrapper from "libs/funcWrapper";
import {getFromLocalStorage, setInLocalStorage} from "libs/webStorage";

export const addNewDict = (dict) => {
	return dispatch => {
		dispatch(actions.AddDictRequest());
		if (!dict.name || !dict.lang) {
			dispatch(actions.AddDictFail('Fields can not be empty'));
			return;
		}
		
		wrapper(() => {
			const authData = getFromLocalStorage('auth');
			if (!authData || !authData.token) {
				dispatch(actions.AddDictFail("Auth required"));
				return;
			}
			fetch('/api/dicts/create', {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json'
				},
				body: JSON.stringify({
					token: authData.token,
					...dict
				}),
			})
			.then(res => res.json())
			.then(json => {
				if (json.success) {
					dispatch(actions.AddDictSuccess(json.dict));
				} else {
					dispatch(actions.AddDictFail(json.message));
				}
			})
			.catch(() => {
				dispatch(actions.AddDictFail("Unknown error"));
			})
		});
	}
};

export const GetDicts = () => {
	return dispatch => {
		dispatch(actions.GetDictsRequest());
		wrapper(() => {
			const authData = getFromLocalStorage('auth');
			if (!authData || !authData.token) {
				dispatch(actions.GetDictsFail("Auth required"));
				return;
			}
			fetch('/api/dicts/getAll?token='+authData.token)
			.then(res => res.json())
			.then(json => {
				if (json.success) {
					dispatch(actions.GetDictsSuccess(json.dicts));
				} else {
					dispatch(actions.GetDictsFail(json.message));
				}
			})
			.catch(() => {
				dispatch(actions.GetDictsFail("Unknown error"));
			})
		});
	}
};