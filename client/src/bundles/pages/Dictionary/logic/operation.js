import * as actions from './action';
import wrapper from "libs/funcWrapper";
import {getFromLocalStorage} from "libs/webStorage";

export const addNewWord = (word) => {
	return dispatch => {
		dispatch(actions.AddWordRequest());
		if (!word.value || !word.translate) {
			dispatch(actions.AddWordFail('Fields can not be empty'));
			return;
		}
		
		wrapper(() => {
			const authData = getFromLocalStorage('auth');
			if (!authData || !authData.token) {
				dispatch(actions.AddWordFail("Auth required"));
				return;
			}
			fetch('/api/words/create', {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json'
				},
				body: JSON.stringify({
					token: authData.token,
					...word
				}),
			})
			.then(res => res.json())
			.then(json => {
				if (json.success) {
					dispatch(actions.AddWordSuccess(json.word));
				} else {
					dispatch(actions.AddWordFail(json.message));
				}
			})
			.catch(() => {
				dispatch(actions.AddWordFail("Unknown error"));
			})
		});
	}
};

export const DeleteWord = (wordId) => {
	return dispatch => {
		dispatch(actions.DeleteWordRequest(wordId));
		wrapper(() => {
			const authData = getFromLocalStorage('auth');
			if (!authData || !authData.token) {
				return;
			}
			fetch('/api/words/delete', {
				method: 'DELETE',
				headers: {
					'Content-Type': 'application/json'
				},
				body: JSON.stringify({
					token: authData.token,
					id: wordId
				}),
			})
			.then(res => res.json())
			.then(json => {
				if (!json.success) {
					console.log("Delete error");
				}
			})
			.catch(() => {
				console.log("Delete error");
			})
		});
	}
};

export const GetWords = (dictId) => {
	return dispatch => {
		dispatch(actions.GetWordRequest());
		wrapper(() => {
			const authData = getFromLocalStorage('auth');
			if (!authData || !authData.token) {
				dispatch(actions.GetWordFail("Auth required"));
				return;
			}
			fetch('/api/words/getAll?token='+authData.token+'&dictId='+dictId)
			.then(res => res.json())
			.then(json => {
				if (json.success) {
					dispatch(actions.GetWordSuccess(json.words));
				} else {
					dispatch(actions.GetWordFail(json.message));
				}
			})
			.catch(() => {
				dispatch(actions.GetWordFail("Unknown error"));
			})
		});
	}
};