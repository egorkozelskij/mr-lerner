import React from 'react';
import PropTypes from "prop-types";
import {Link, Redirect, Route, Switch} from "react-router-dom";

import MainWrapper2 from "template/MainWrapper2";
import {routeTo} from "config/routes";
import AllWords from "./filterComponents/AllWords";
import Favorites from "./filterComponents/Favorites";
import WordSets from "./filterComponents/WordSets";

import './style.scss';

const DictionaryPresenter = ({dictId, option, path}) => {
	const TopComponent = () => (
		<Link className="nav-link top-bar-arrow" to={routeTo("dicts")}>
			<i className="fas fa-arrow-left"/> Back to Dictionaries
		</Link>
	);
	const LeftComponent = () => {
		const url = `${path}/${dictId}`;
		return (
			<ul className="nav flex-column left-bar">
				<li className="nav-item">
					<Link className={`nav-link ${option==='favorites'?'active':''}`}  to={`${url}/favorites`}>
						<i className="fas fa-star"/> Favorites
					</Link>
				</li>
				<li className="nav-item">
					<Link className={`nav-link ${option==='all_words' || !option?'active':''}`}  to={`${url}/all_words`}>
						<i className="fas fa-globe-americas"/> All words
					</Link>
				</li>
				<li className="nav-item">
					<Link className={`nav-link ${option==='word_sets'?'active':''}`}  to={`${url}/word_sets`}>
						<i className="fas fa-box"/> Word sets
					</Link>
				</li>
			</ul>
		);
	};
	const MiddleComponent = () => {
		if (option === undefined || option === 'all_words')
			return <AllWords dictId={dictId}/>;
		
		if (option === 'favorites')
			return <Favorites/>;
	
		if (option === 'word_sets')
			return <WordSets/>;
			
		return (<Redirect to='/404'/>);
	};
	
	return (
		<MainWrapper2 LeftComponent={LeftComponent}
		              TopComponent={TopComponent}
		              MiddleComponent={MiddleComponent}/>
	);
};
DictionaryPresenter.propTypes = {
	dictId: PropTypes.string.isRequired,
	option: PropTypes.string,
	path: PropTypes.string.isRequired
};
export default DictionaryPresenter;


