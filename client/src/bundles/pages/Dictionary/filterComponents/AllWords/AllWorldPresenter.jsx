import React from 'react';
import PropTypes from 'prop-types';
import {connect} from "react-redux";

import './style.scss';

const AllWorldPresenter = ({dictId, words, onDelete}) => (
	<React.Fragment>
		<div className="container-fluid p-0 mb-2 dict_word-panel d-flex flex-row">
			<div className='col col-6'>
				<label className="checkbox">
					<input type="checkbox" className="custom-checkbox"/>
					<span className="icons">
			        <span className="icon-unchecked"/>
				        <span className="icon-checked"/>
			        </span>
					{/*Check all*/}
				</label>
			</div>
			<div className='col col-6 p-0 d-flex flex-row-reverse'>
				<button type="button" className="btn add-button">Add word</button>
			</div>
		</div>
		<ul className="list-group dict_word-box">
			{words.data.map((word, key) => (
				<li key={key} className="list-group-item d-inline-flex">
					<div className='col col-11 first'>
						<label className="checkbox">
							<input type="checkbox" className="custom-checkbox"/>
							<span className="icons">
					        <span className="icon-unchecked"/>
						        <span className="icon-checked"/>
					        </span>
						</label>
						<div className='dict_word-wrapper d-inline-flex'>
							<div className="dict_word">{word.value}</div>
							<div className="dict_sep">------</div>
							<div className="dict_translate">{word.translate}</div>
						</div>
					</div>
					<div className='col col-1 dict_box-left-block d-flex flex-row-reverse'>
						<div className='trash'>
							<button className='btn btn-link'
									onClick={() => {onDelete(word._id)}}>
								<i className="fas fa-trash-alt"/>
							</button>
						</div>
					</div>
				</li>
			))}
		</ul>
	</React.Fragment>
);
AllWorldPresenter.propTypes = {
	dictId: PropTypes.string.isRequired,
	words: PropTypes.shape({
		isFetching: PropTypes.bool.isRequired,
		data: PropTypes.arrayOf(PropTypes.shape({
			value: PropTypes.string.isRequired,
			translate: PropTypes.string.isRequired,
			_id: PropTypes.string.isRequired
		})).isRequired
	}).isRequired,
	onDelete: PropTypes.func.isRequired
};
const mapStateToProps = (state) => {
	return {
		words: state.words
	};
};
export default connect(mapStateToProps, null)(AllWorldPresenter);