import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {bindActionCreators} from "redux";
import {connect} from "react-redux";

import AllWorldPresenter from "./AllWorldPresenter";
import {DeleteWord, GetWords} from "../../logic/operation";

class AllWords extends Component {
	constructor(props) {
		super(props);
		this.onDelete = this.onDelete.bind(this);
	}
	
	componentDidMount() {
		this.props.GetWords(this.props.dictId);
	}
	
	onDelete(wordId) {
		this.props.DeleteWord(wordId);
	}
	
    render() {
        return (
			<AllWorldPresenter dictId={this.props.dictId} onDelete={this.onDelete}/>
        );
    }
}
AllWords.propTypes = {
	dictId: PropTypes.string.isRequired,
	GetWords: PropTypes.func.isRequired,
	DeleteWord: PropTypes.func.isRequired
};
const mapDispatchToProps = (dispatch) => {
	return bindActionCreators({
		GetWords,
		DeleteWord
	}, dispatch);
};
export default connect(null, mapDispatchToProps)(AllWords);
