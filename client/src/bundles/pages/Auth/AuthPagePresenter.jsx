import React from 'react';
import {connect} from "react-redux";
import {Col, Container} from "reactstrap";
import PropTypes from "prop-types";

import SignIn from './components/SignIn';
import SignUp from './components/SignUp';
import Message from 'common/Message';
import './style.scss';

const AuthPagePresenter = ({active, onTabChange, auth, ...events}) => (
	<Container fluid className="d-flex justify-content-center bg-main-wrapper">
		<Col xs={12} sm={10} md={8} lg={6} xl={4} className='mt-5'>
			<ul className="nav nav-tabs login-tabs">
				<li className="nav-item">
					<a className={"nav-link " + (active === 0 ? 'active' : '')}
					   onClick={() => onTabChange(0)}>Sign In</a>
				</li>
				<li className="nav-item">
					<a className={"nav-link " + (active === 1 ? 'active' : '')}
					   onClick={() => onTabChange(1)}>Sign Up</a>
				</li>
			</ul>
			<form className="login-form" action='/' onSubmit={(e) => {
				return (active === 0) ? events.onSignIn(e) : events.onSignUp(e) ;
			}}>
				{active === 0 ? <SignIn auth={auth} {...events}/> : <SignUp auth={auth} {...events}/>}
				<div className='mt-3'>
					{(auth.signInSuccess === false || (auth.signUpSuccess === false)) ?
						<Message type='danger' message={auth.errorMessage}/>
						: ''
					}
				</div>
			</form>
		</Col>
	</Container>
 );
AuthPagePresenter.propTypes = {
	active: PropTypes.number.isRequired,
	onTabChange: PropTypes.func.isRequired,
	onSignIn: PropTypes.func.isRequired,
	onSignUp: PropTypes.func.isRequired,
	auth: PropTypes.object.isRequired
};
const mapStateToProps = (state) => {
	return {
		auth: state.auth,
	};
};
export default connect(mapStateToProps, null)(AuthPagePresenter);
