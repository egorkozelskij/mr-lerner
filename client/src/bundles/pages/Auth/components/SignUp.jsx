import React from 'react';
import PropTypes from 'prop-types';

import Loader from 'common/Loader';
import {connect} from "react-redux";

const SignUp = ({auth, ...events}) => (
	<React.Fragment>
		<div className="form-group">
			<input type="email"
			       className="form-control login-field"
			       placeholder="Enter your email"
			       id="login-name"
			       onChange={(e) => events.onSignUpEmailChange(e.target.value)}/>
			<label className="login-field-icon fui-user" htmlFor="login-name"/>
		</div>
		
		<div className="form-group">
			<input type="password"
			       className="form-control login-field"
			       placeholder="Password"
			       id="login-pass"
			       onChange={(e) => events.onSignUpPassChange(e.target.value)}/>
			<label className="login-field-icon fui-lock" htmlFor="login-pass"/>
		</div>
		
		<div className="form-group">
			<input type="password"
			       className="form-control login-field"
			       placeholder="Password again"
			       id="signUp-login-pass"
			       onChange={(e) => events.onSignUpPassAgainChange(e.target.value)}/>
			<label className="login-field-icon fui-lock" htmlFor="signUp-login-pass"/>
		</div>
		
		<button className="btn btn-primary btn-lg btn-block"
		        onClick={events.onSignUp}
		        disabled={auth.isFetching}>
			{!auth.isFetching ? ('Sign up') : <Loader size='30px'/>}
		</button>
	</React.Fragment>
);
SignUp.propTypes = {
	onSignUpPassChange: PropTypes.func.isRequired,
	onSignUpPassAgainChange: PropTypes.func.isRequired,
	onSignUpEmailChange : PropTypes.func.isRequired,
	onSignUp: PropTypes.func.isRequired,
	auth: PropTypes.object.isRequired
};
export default SignUp;
