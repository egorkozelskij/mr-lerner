import React from 'react';
import PropTypes from 'prop-types';

import Loader from 'common/Loader';
import {connect} from "react-redux";

const SignIn = ({auth, ...events}) => (
	<React.Fragment>
		<div className="form-group">
			<input type="email"
			       className="form-control login-field"
			       placeholder="Enter your email"
			       id="login-name"
			       onChange={(e) => events.onSignInEmailChange(e.target.value)}/>
			<label className="login-field-icon fui-user" htmlFor="login-name"/>
		</div>
		
		<div className="form-group">
			<input type="password"
			       className="form-control login-field"
			       placeholder="Password"
			       id="signIn-login-pass"
			       onChange={(e) => events.onSignInPassChange(e.target.value)}/>
			<label className="login-field-icon fui-lock" htmlFor="signIn-login-pass"/>
		</div>
		
		<button className="btn btn-primary btn-lg btn-block"
		        onClick={events.onSignIn}
		        disabled={auth.isFetching}>
			{!auth.isFetching ? ('Log in') : <Loader size='30px'/>}
		</button>
		<a className="login-link" href="#">Lost your password?</a>
	</React.Fragment>
 );
SignIn.propTypes = {
	onSignInEmailChange: PropTypes.func.isRequired,
	onSignInPassChange: PropTypes.func.isRequired,
	onSignIn: PropTypes.func.isRequired,
	auth: PropTypes.object.isRequired
};
export default SignIn;

