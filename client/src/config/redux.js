import {createStore, applyMiddleware, combineReducers} from "redux";
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';

import authReducer from 'logic/auth/reducer';
import {dictReducer} from "pages/DictionaryList";
import wordReducer from "pages/Dictionary/logic/reducer";

const rootReducer = combineReducers({
	auth: authReducer,
	dicts: dictReducer,
	words: wordReducer
});

let store = createStore(
	rootReducer,
	composeWithDevTools(
		applyMiddleware(thunk)
	)
);
export default store;