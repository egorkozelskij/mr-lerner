const path = require('path');
const webpack = require("webpack");
const HtmlWebpackPlugin = require( 'html-webpack-plugin' );
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const extractCSS = new ExtractTextPlugin('styles.min.css');

module.exports = {
	devtool: 'cheap-eval-source-map',
	entry: {
		path: path.resolve(__dirname, 'src')
	},
	output: {
		path: path.resolve(__dirname, 'public'),
		filename: 'main.min.js',
		publicPath: "/"
	},
	module: {
		rules: [
			{
				test: /\.js$|\.es6|\.jsx$/,
				resolve: {
					extensions: ['.es6', '.js', '.jsx']
				},
				exclude: /node_modules/,
				use: {
					loader: "babel-loader"
				}
			},
			{
				test: /\.css$|\.scss$/,
				resolve: {
					extensions: ['.css', '.scss']
				},
				use: extractCSS.extract({
					fallback: 'style-loader',
					use: [
							{
								loader: 'css-loader'
							},
							{
								loader: 'postcss-loader',
								options: {
									sourceMap: false
								}
							}
						],
				})
			},
			{
				test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
				use: [{
					loader: 'file-loader',
					options: {
						name: '[name].[ext]',
						outputPath: 'fonts/'
					}
				}]
			},
			{
				test: /\.png$/,
				use: [{
					loader: 'file-loader',
					options: {
						name: '[name].[ext]',
						outputPath: 'images/'
					}
				}]
			},
			{
				test: /\.jpg/,
				use: [{
					loader: 'file-loader',
					options: {
						name: '[name].[ext]',
						outputPath: 'images/'
					}
				}]
			}
		]
	},
	plugins: [
		// new HtmlWebpackPlugin({
		// 	template: path.resolve('src', 'index.html')
		// }),
		extractCSS
	],
	// optimization: {
	// 	minimize: true,
	// 	minimizer: [new UglifyJsPlugin({
	// 		include: /\.min\.js$/
	// 	})]
	// },
	devServer: {
		port: 3000,
		proxy: {
			'*': 'http://localhost:8080'
		}
	}
};


