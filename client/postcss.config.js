module.exports = {
	parser: 'postcss-scss',
	plugins: {
		'autoprefixer': {},
		'cssnano': {},
		'postcss-import': {
			path: ['./src/bundles/template/styles']
		},
		'postcss-nested': {},
		'postcss-mixins': {},
		'postcss-simple-vars': {}
	}
};