const mongoose = require('mongoose');

const UserSessionSchema = new mongoose.Schema({
	userId: {
		type: String,
		default: '',
		unique: true
	},
	timestamp: {
		type: Date,
		default: Date.now()
	}
});

module.exports = mongoose.model('UserSession', UserSessionSchema);