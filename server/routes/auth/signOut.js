const path = require('path');
const express = require('express');
const router = express.Router();
const UserSession = require(path.resolve('models/UserSession'));
const {wrapper, serverSuccess} = require(path.resolve('utils/response'));

router.get('/signout', wrapper( async (req, res, next) => {
	const doc = await UserSession.findOne({_id: req.query.token});
	if (doc)
		await doc.remove();
	return serverSuccess();
}));

module.exports = router;
