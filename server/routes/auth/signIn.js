const express = require('express');
const router = express.Router();
const path = require('path');
const User = require(path.resolve('models/User'));
const UserSession = require(path.resolve('models/UserSession'));
const {wrapper, serverSuccess} = require(path.resolve('utils/response'));

router.post('/signin', wrapper(async (req, res, next) => {
	let {email, password} = req.body;
	if(!email || !password)
		throw new Error('Error: email or password cannot be blank');
	
	email = email.toLowerCase().trim();
	const user = await User.findOne({email: email});
	
	if (!user)
		throw new Error('Error: User not found');
	if (!user.validPassword(password))
		throw new Error('Error: Invalid');
	
	const doc = await UserSession.findOne({userId: user._id});
	if (doc)
		await doc.remove();
	
	const userSession = new UserSession();
	userSession.userId = user._id;
	await userSession.save();
	return serverSuccess({
		token: userSession._id,
		user: {
			email: user.email,
			signUpDate: user.signUpDate
		}
	});
}));

module.exports = router;

