const express = require('express');
const router = express.Router();

router.use('/', require('./signIn'));
router.use('/', require('./signUp'));
router.use('/', require('./signOut'));
router.use('/', require('./verify'));
router.use('/', require('./userData'));

module.exports = router;