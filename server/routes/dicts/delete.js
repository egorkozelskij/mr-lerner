const express = require('express');
const router = express.Router();
const path = require('path');
const validate = require(path.resolve('utils/authValidation'));
const Dict = require(path.resolve('models/Dict'));
const {wrapper, serverSuccess} = require(path.resolve('utils/response'));

router.delete('/delete', wrapper(async (req, res, next) => {
	let {token, id} = req.body;
	const result = await validate(token);
	if (!result.success)
		throw new Error(result.message);
	
	const deleteResult = await Dict.find({_id: id});
	if (!deleteResult)
		throw new Error('Error: dictionary not found');
	deleteResult.remove();
	return serverSuccess();
}));

module.exports = router;

