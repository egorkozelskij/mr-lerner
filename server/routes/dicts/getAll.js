const express = require('express');
const router = express.Router();
const path = require('path');
const validate = require(path.resolve('utils/authValidation'));
const Dict = require(path.resolve('models/Dict'));
const {wrapper, serverSuccess} = require(path.resolve('utils/response'));

router.get('/getAll', wrapper(async (req, res, next) => {
	const result = await validate(req.query.token);
	if (!result.success)
		throw new Error(result.message);

	const dicts = await Dict.find({userId: result.user._id});
	return serverSuccess({dicts});
}));

module.exports = router;

