const express    = require('express');
const bodyParser = require('body-parser');
const config     = require('./config/config');
const path       = require('path');

const app = express();

//STATIC
app.use(express.static(path.join(__dirname, '..', 'client', 'public')));

//BODY PARSER
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
	extended: false
}));

//CONFIG
require('./config')(app);

//RUN SERVER
app.use('/api', require('./routes'));
app.get('/*', function (req, res) {
	res.sendFile(path.join(__dirname, '..', 'client', 'public', 'index.html'));
});

app.use((req, res) => {
	res.send('Error 404');
});

app.listen(config.port, () => {
	console.log('Server started on port ' + config.port);
});
