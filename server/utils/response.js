const serverError = (message='Error: Server error') => {
	return {
		success: false,
		message: message
	};
};

const serverSuccess = (state = {}) => {
	return Object.assign({success: true}, state);
};

const wrapper = (fn) => {
	return function(req, res, next) {
		fn(req,res,next).then(returnVal => res.send(returnVal))
		.catch((err) => {
			return res.send(serverError(err.message));
		});
	};
};

module.exports = {
	serverSuccess,
	serverError,
	wrapper
};