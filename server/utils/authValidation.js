const ObjectId = require('mongoose').Types.ObjectId;
const path = require('path');
const UserSession = require(path.resolve('models/UserSession'));
const User = require(path.resolve('models/User'));

const validate = async (token) => {
	if (!token || !ObjectId.isValid(token))
		return {
			success: false,
			message: 'Token invalid'
		};
	
	const doc = await UserSession.findOne({_id: token});
	if (!doc)
		return {
			success: false,
			message: 'Token invalid'
		};
	
	const user = await User.findOne({_id: doc.userId}).select({"email":1, "signUpDate":1});
	if (!user)
		return {
			success: false,
			message: 'Token invalid'
		};
	return {
		success: true,
		user: user
	};
};

module.exports = validate;